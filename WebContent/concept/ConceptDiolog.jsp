<div id="mainDiologContainer" class="mainDiologContainerStyle">
  <div id="conceptSubDiologPanel" class="conceptSubDiologPanelStyle"></div>
  <div id="conceptDiologPanel" class="conceptDiologPanelStyle">
   <center>
    <fieldset style="width:330px; height:170px">
    <legend>
      <b style="font-size:17px; text-align:left">Please Select Abstract Type</b><br /><br />
    </legend>
      <font style="font-size: 15px; text-decoration: underline;">Choose Type</font><br/>	
      <select id="conceptType" style="width:200px">
        <option></option>
        <option>Primitive Abstract</option>            
        <option>Composite Abstract</option>
        <option>Set</option>
        <option>Action</option>                    
      </select><br /><br />
      
      <div class="parentBunchClassPanelStyle" id="parentBunchClassPanel">
			<font style="font-size: 15px; text-decoration: underline">Initial Abstract</font>
      </div>
      
      <input type="button" value="Ok" id="OkButton" class="dialogButtons"/>
      <input type="button" value="Cancel" id="CancelButton" class="dialogButtons" />
    
    </fieldset>
    </center>
   </div> 
</div>  