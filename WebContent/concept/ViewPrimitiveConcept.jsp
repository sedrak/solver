	<div class="primTitleBar">
	<div class="closeNuc closeicon">
	</div>
	<div class="helpNuc helpicon">
	</div>
	</div>
	<div class="primitiveConceptNameField primitiveConceptNameFieldStyle" id="primitiveConceptNameField" contenteditable="true">
		New Primitive Abstract
	</div>  
	
	<div class="parentConceptPanel">
		<select id="primitiveParent" class="primitiveParent"></select>
	</div>
	  
	<div id="primitiveConceptsAttrsContainer" class="primitiveConceptsAttrsContainer">	
	<div class="indexAttributeHeader indexAttributeHeaderStyle">
		<div class="indexAttributeAdminFieldName">Index Attributes</div>
		<div class="indexAttributeAdminFieldButton" id="indexAttributeAdminFieldButton">
			<input type="button" value="Add Index Attributes" id="addPrimIndexAttr" style="font-size: 10px;">
		</div>
	</div>
	<div class="indexAttrContainer indexAttrContainerStyle" id="indexAttrContainer">
	
	</div>
	
	<div class="valueAttributeHeader valueAttributeHeaderStyle">
		<div class="valueAttributeAdminFieldName">Value Attributes</div>
		<div class="valueAttributeAdminFieldButton" id="valueAttributeAdminFieldButton">
			<input type="button" value="Add Value Attributes" id="addPrimValueAttr" style="font-size: 10px;">
		</div>
	</div>
	<div class="valueAttrContainer valueAttrContainerStyle" id="valueAttrContainer">
	
	</div>
</div> 