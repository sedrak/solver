<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:property value="NucleusList"/>
<s:property value="NucleusConc"/>
<s:property value="PrimitiveList"/>
<s:property value="PrimitiveConc"/>
<s:property value="ConceptList"/>
<s:property value="compositeConc"/>
<s:property value="actionList"/>
<s:property value="actionConc"/>
<s:property value="setList"/>
<s:property value="set"/>
<s:property value="activatedAbstracts"/>
<s:property value="nextInstance"/>
<s:property value="GoalData"/>
<s:property value="GoalList"/>
<s:property value="PlanData"/>
<s:property value="PlanList"/>
<s:property value="PlanWrapperList"/>
<s:property value="PlanWrapperData"/>
<s:property value="NextSuggestedInstance"/>
