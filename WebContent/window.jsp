<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>SSRGT &trade;</title>

<script type="text/javascript" src="static/js/lib/jquery.js"></script>
<script type="text/javascript" src="static/js/lib/jquery.json-2.2.js"></script>
<script type="text/javascript" src="static/js/lib/jquery-ui.js"></script>
<script type="text/javascript" src="static/js/lib/jquery.showLoading.js"></script>
<script type="text/javascript" src="static/js/util.js"></script>
<script type="text/javascript" src="static/js/TabPanel.js"></script>
<script type="text/javascript" src="static/js/nucConcept.js"></script>
<script type="text/javascript" src="static/js/ConceptsManager.js"></script>
<script type="text/javascript" src="static/js/primConcept.js"></script>
<script type="text/javascript" src="static/js/comConcept.js"></script>
<script type="text/javascript" src="static/js/Set.js"></script>
<script type="text/javascript" src="static/js/Action.js"></script>
<script type="text/javascript" src="static/js/chessSituation.js"></script>
<script type="text/javascript" src="static/js/plans.js"></script>

<!-- <link rel="stylesheet" type="text/css" href="static/css/lib/jquery-ui.css" /> -->
<link rel="stylesheet" type="text/css" href="static/css/PageStyle.css" />
<link rel="stylesheet" type="text/css" href="static/css/SetStyle.css">
<link rel="stylesheet" type="text/css" href="static/css/PercContentStyle.css" /> 
<link rel="stylesheet" type="text/css" href="static/css/ConceptsStyle.css" />
<link rel="stylesheet" type="text/css" href="static/css/SitContentStyle.css" />
<link rel="stylesheet" type="text/css" href="static/css/BunchStyle.css" />
<link rel="stylesheet" type="text/css" href="static/css/ConceptDiologStyle.css" />
<link rel="stylesheet" type="text/css" href="static/css/PrimitiveConceptStyle.css" />
<link rel="stylesheet" type="text/css" href="static/css/ConceptsTreeStyle.css" />
<link rel="stylesheet" type="text/css" href="static/css/CompositeConceptStyle.css"> 
<link rel="stylesheet" type="text/css" href="static/css/ChessContentStyle.css"> 
<link rel="stylesheet" type="text/css" href="static/css/ActionContainerStyle.css"> 
<link rel="stylesheet" type="text/css" href="static/css/ActionDialogPanalStyle.css">
<link rel="stylesheet" type="text/css" href="static/css/ChessSituation.css" />
<link rel="stylesheet" type="text/css" href="static/css/PlansStyle.css" />

<style type="text/css">

</style>

</head>


<body>
	
	<div id="mainPanel" class="mainPanelStyle">
	  <div class="topPanel">
		<div id="tabs" class="tabGroupStyle">
			<div id="tab1" class="tabs tabStyle">Home</div>
			<div id="tab2" class="tabs tabStyle">Nucleus Abstracts</div>
			<div id="tab3" class="tabs tabStyle">Abstracts</div>
			<div id="tab4" class="tabs tabStyle">Chess</div>
			<div id="tab5" class="tabs tabStyle">Market</div>
			<div id="tab6" class="tabs tabStyle">Graph of Abstracts</div>
			<div id="tab7" class="tabs tabStyle">Plans</div>
		</div>
	  </div>
		<div id="contentGroup" class="contentGroupStyle">
			
		</div>
		
	</div>
</body>
</html>