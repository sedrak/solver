package org.ppit.test.concept.expression.analyze;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ InfixToPostfixTest.class, SprlitExpressionTest.class })
public class AllTests {

}
